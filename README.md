# FaceApp API ![](https://gitlab.com/lolPants/faceapp-api/badges/master/build.svg)
_Simple API Server for [faceapp.js](https://github.com/lolPants/faceapp.js)_  
Built by [Jack Baron](https://www.jackbaron.com)

## Usage
### Prerequisites
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Docker Registry
More info is available [here](https://gitlab.com/lolPants/faceapp-api/container_registry).

### Development
Start the service using `docker-compose up`  
It wil start running in the foreground. `CTRL+C` to stop. You can also run it in detatched mode with `docker-compose up -d`.  
When you're done, `docker-compose rm` to remove the leftover containers.

### Production
Start the service in detached mode with autoreload with `docker-compose -f docker-compose.yml -f production.yml up -d`

## Maintainers
- lolPants (https://www.jackbaron.com)
