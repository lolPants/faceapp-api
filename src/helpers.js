/**
 * Allowed file types for uploaded images
 * @type {string[]}
 */
const FILE_TYPES = [
  '.png',
  '.jpg',
  '.jpeg',
]

/**
 * API v1.0 Base URL
 * @type {string}
 */
const API_V1_URL = '/api/v1.0'

/**
 * Checks if a value exists in an array
 * @param {*} test Value to test
 * @param {Array} array Array to test in
 * @returns {boolean}
 */
const inArray = (test, array) => array.filter(a => a === test).length !== 0

module.exports = {
  FILE_TYPES,
  API_V1_URL,
  inArray,
}
