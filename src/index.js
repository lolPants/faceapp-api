// Package Dependencies
const morgan = require('morgan')
const cors = require('cors')
const express = require('express')
const app = express()

// Local Dependencies
const helpers = require('./helpers')

// Standard Middleware
app.use(morgan('combined'))
app.use(cors())

// Local Middleware
const root = require('./root')
const listFilters = require('./listFilters')
const processURL = require('./processURL')
const processPost = require('./processPost')
app.use('/', root)
app.use(helpers.API_V1_URL, listFilters)
app.use(helpers.API_V1_URL, processURL)
app.use(helpers.API_V1_URL, processPost)

app.listen(process.env.PORT || 3000)
