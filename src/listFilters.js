// Package Dependencies
const faceapp = require('faceapp')
const routeCache = require('route-cache')

// Express App
const express = require('express')
const app = express()

app.get('/filters', routeCache.cacheSeconds(60 * 5), async (req, res) => { res.send(await faceapp.listFilters()) })
app.get('/filters/raw', routeCache.cacheSeconds(60 * 5), async (req, res) => { res.send(await faceapp.listFilters(true)) })

module.exports = app
