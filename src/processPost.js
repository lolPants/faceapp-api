// Package Dependencies
const faceapp = require('faceapp')
const path = require('path')
const multer = require('multer')
const upload = multer({ storage: multer.memoryStorage() })

// Express App
const express = require('express')
const app = express()

// Local Dependencies
const helpers = require('./helpers')

app.post('/process/:filter', upload.single('file'), async (req, res) => {
  // File
  let file = req.file

  if (file === undefined || file === null) {
    res.status(400).send({
      status: 400,
      error: 'POST body \'file\' is missing',
    })
    return
  }

  /**
   * @type {string}
   */
  let fileName = file.originalname

  /**
   * @type {string}
   */
  let fileExtension = path.extname(require('url').parse(fileName).pathname)

  /**
   * @type {string}
   */
  let filter = req.params.filter

  /**
   * @type {string[]}
   */
  let KNOWN_FILTERS = await faceapp.listFilters(true)

  if (!helpers.inArray(filter, KNOWN_FILTERS)) {
    let list = KNOWN_FILTERS.join(', ')
    res.status(404).send({
      status: 404,
      error: `Filter ID not found.\nAvailable filter IDs are: ${list}`,
    })
  } else if (!helpers.inArray(fileExtension, helpers.FILE_TYPES)) {
    res.status(400).send({
      status: 400,
      error: 'Uploaded file has an invalid file extension. Please use .png .jpg or .jpeg',
    })
  } else {
    try {
      let data = await faceapp.process(file.buffer, filter)
      await res.set('Content-type', 'image/png')
        .send(data)
    } catch (err) {
      if (err.message === 'No Faces found in Photo') {
        res.status(404).send({
          status: 404,
          error: 'No faces found in the photo',
        })
      } else {
        res.status(500).send({
          status: 500,
          error: 'Unknown server error',
        })
      }
    }
  }
})

module.exports = app
