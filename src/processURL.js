// Package Dependencies
const faceapp = require('faceapp')
const snekfetch = require('snekfetch')
const path = require('path')
const valid = require('valid-url')

// Express App
const express = require('express')
const app = express()

// Local Dependencies
const helpers = require('./helpers')

app.get('/process/:filter', async (req, res) => {
  /**
   * @type {string}
   */
  let url = req.query.url

  if (!url) {
    return res.status(400).send({
      status: 400,
      error: 'URL paramter must be specified',
    })
  }

  /**
   * @type {string}
   */
  let fileExtension = path.extname(require('url').parse(url).pathname)

  /**
   * @type {string}
   */
  let filter = req.params.filter

  /**
   * @type {string[]}
   */
  let KNOWN_FILTERS = await faceapp.listFilters(true)

  if (!helpers.inArray(filter, KNOWN_FILTERS)) {
    let list = KNOWN_FILTERS.join(', ')
    res.status(404).send({
      status: 404,
      error: `Filter ID not found.\nAvailable filter IDs are: ${list}`,
    })
  } else if (!valid.isWebUri(url)) {
    res.status(400).send({
      status: 400,
      error: 'URL paramter is not a valid URL',
    })
  } else if (!helpers.inArray(fileExtension, helpers.FILE_TYPES)) {
    res.status(400).send({
      status: 400,
      error: 'URL has an invalid file extension. Please use .png .jpg or .jpeg',
    })
  } else {
    let resp = await snekfetch.get(url)
    try {
      let data = await faceapp.process(resp.body, filter)
      await res.set('Content-type', 'image/png')
        .send(data)
    } catch (err) {
      if (err.message === 'No Faces found in Photo') {
        res.status(404).send({
          status: 404,
          error: 'No faces found in the photo',
        })
      } else {
        res.status(500).send({
          status: 500,
          error: 'Unknown server error',
        })
      }
    }
  }
})

module.exports = app
