// Package Dependencies
const routeCache = require('route-cache')

// Local Dependencies
const helpers = require('./helpers')

// Express App
const express = require('express')
const app = express()

app.get('/', routeCache.cacheSeconds(60 * 60), (req, res) => {
  let text = [
    'FaceApp API Version 1.0',
    '',
    'List all Filters',
    `GET ${helpers.API_V1_URL}/filters`,
    '',
    'List all Filter IDs',
    `GET ${helpers.API_V1_URL}/filters/raw`,
    '',
    'Process an Image URL',
    `GET ${helpers.API_V1_URL}/process/:filter`,
    'Query:\turl=[image url]',
    '',
    'Process an Image Upload',
    `POST ${helpers.API_V1_URL}/process/:filter`,
    'Body:\tfile=[image file]',
  ]

  res.set('Content-type', 'text/plain')
    .send(text.join('\n'))
})

// Robots TXT
app.get('/robots.txt', routeCache.cacheSeconds(60 * 60), (req, res) => {
  let PATH = require('path').join(__dirname, 'public', 'robots.txt')
  res.sendFile(PATH)
})

module.exports = app
